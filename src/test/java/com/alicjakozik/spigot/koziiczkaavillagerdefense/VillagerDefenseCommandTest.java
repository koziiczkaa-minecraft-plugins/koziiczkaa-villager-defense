package com.alicjakozik.spigot.koziiczkaavillagerdefense;

import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.MobSpawnLocation;
import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.VillagerDefenseCuboidRegion;
import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.VillagerDefenseGame;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.testng.MockitoTestNGListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNGListener.class)
public class VillagerDefenseCommandTest {
	@Mock
	private VillagerDefensePlugin plugin;
	@Mock
	private Player player;
	@Mock
	private World world;
	@Mock
	private Villager villager;
	@Mock
	private Region region;
	@Mock
	private LocalSession localSession;

	@Test
	void testVillagerSuccessfullyAdded() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(BlockVector3.at(3, 65, 5),
				BlockVector3.at(10, 70, 10));
		final VillagerDefenseGame villagerDefenseGame = new VillagerDefenseGame(cuboidRegion, "test", 10);
		final Location location = new Location(world, 6, 65, 6);
		when(player.getLocation()).thenReturn(location);
		when(world.spawn(location, Villager.class)).thenReturn(villager);

		// when
		final String result = command.villager(villagerDefenseGame);

		// then
		verify(villager).setMetadata(eq("villagerDefenseGame"), argThat(value ->
				value.asString().equals("test") && Objects.equals(value.getOwningPlugin(), plugin)));
		verify(villager).setAI(false);
		assertThat(villagerDefenseGame.getVillager()).isSameAs(villager);
		assertThat(result).isEqualTo("&aSuccessfully added villager to the game!");
	}

	@Test
	void testVillagerSuccessfullyAddedWithVillager() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(BlockVector3.at(3, 65, 5),
				BlockVector3.at(10, 70, 10));
		final VillagerDefenseGame villagerDefenseGame = new VillagerDefenseGame(cuboidRegion, "test", 10);
		final Location location = new Location(world, 6, 65, 6);
		final Villager villagerMock = mock(Villager.class);
		villagerDefenseGame.setVillager(villagerMock);
		when(player.getLocation()).thenReturn(location);
		when(world.spawn(location, Villager.class)).thenReturn(villager);

		// when
		final String result = command.villager(villagerDefenseGame);

		// then
		verify(villager).setMetadata(eq("villagerDefenseGame"), argThat(value ->
				value.asString().equals("test") && Objects.equals(value.getOwningPlugin(), plugin)));
		verify(villager).setAI(false);
		assertThat(villagerDefenseGame.getVillager()).isNotSameAs(villagerMock);
		assertThat(result).isEqualTo("&aSuccessfully added villager to the game!");
	}

	@Test
	void testVillagerNoWorld() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(BlockVector3.at(3, 65, 5),
				BlockVector3.at(10, 70, 10));
		final VillagerDefenseGame villagerDefenseGame = new VillagerDefenseGame(cuboidRegion, "test", 10);
		final Location location = new Location(null, 6, 65, 6);
		when(player.getLocation()).thenReturn(location);

		// when
		final String result = command.villager(villagerDefenseGame);

		// then
		assertThat(result).isEqualTo("&4Error: &cWorld doesn't exist");
	}

	@Test
	void testVillagerPlayerNotInRegion() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(BlockVector3.at(3, 65, 5),
				BlockVector3.at(10, 70, 10));
		final VillagerDefenseGame villagerDefenseGame = new VillagerDefenseGame(cuboidRegion, "test", 10);
		villagerDefenseGame.setVillager(villager);
		final Location location = new Location(world, 6, 65, 3);
		when(player.getLocation()).thenReturn(location);

		// when
		final String result = command.villager(villagerDefenseGame);

		// then
		assertThat(result).isEqualTo("&4Error: &cYou have to be in the game's region to perform this command!");
	}

	@Test
	void testVillagerDefenseGameFallback() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);

		// when
		final String result = command.villagerDefenseGameFallback("test");

		// then
		assertThat(result).isEqualTo("&4Error: &cCouldn't find game called &6test");
	}

	@Test
	void testCreateSuccessfully() throws IncompleteRegionException {
		try (final MockedStatic<Bukkit> bukkitMock = mockStatic(Bukkit.class)) {
			// given
			final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
			bukkitMock.when(Bukkit::getVersion).thenReturn("(MC: 1.19.4)");
			try (final MockedStatic<PaperLib> paperLibMock = mockStatic(PaperLib.class);
				 final MockedStatic<BukkitAdapter> bukkitAdapterMock = mockStatic(BukkitAdapter.class)) {
				paperLibMock.when(PaperLib::getMinecraftVersion).thenReturn(20);
				final com.sk89q.worldedit.world.World worldEditWorld = mock(com.sk89q.worldedit.world.World.class);
				when(worldEditWorld.getMinY()).thenReturn(-64);
				when(worldEditWorld.getMaxY()).thenReturn(320);
				bukkitAdapterMock.when(() -> BukkitAdapter.adapt(world)).thenReturn(worldEditWorld);
				final WorldEditPlugin worldEditPlugin = mock(WorldEditPlugin.class);
				when(plugin.getWorldEdit()).thenReturn(worldEditPlugin);
				when(worldEditPlugin.getSession(player)).thenReturn(localSession);
				when(player.getWorld()).thenReturn(world);
				when(localSession.getSelection(worldEditWorld)).thenReturn(region);
				final BlockVector3 minimum = BlockVector3.at(0, 0, 0);
				final BlockVector3 maximum = BlockVector3.at(8, 8, 8);
				when(region.getMaximumPoint()).thenReturn(maximum);
				when(region.getMinimumPoint()).thenReturn(minimum);
				final List<VillagerDefenseGame> games = new ArrayList<>();
				when(plugin.getGames()).thenReturn(games);

				// when
				final String result = command.create("test", 5);

				// then
				assertThat(games).containsExactly(new VillagerDefenseGame(
						new VillagerDefenseCuboidRegion(maximum, minimum), "test", 5));
				assertThat(result).isEqualTo("&aSuccessfully created region!");
			}
		}
	}

	@Test
	void testCreateIncompleteRegion() throws IncompleteRegionException {
		try (final MockedStatic<Bukkit> bukkitMock = mockStatic(Bukkit.class)) {
			// given
			final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
			bukkitMock.when(Bukkit::getVersion).thenReturn("(MC: 1.20.2)");
			try (final MockedStatic<PaperLib> paperLibMock = mockStatic(PaperLib.class);
				 final MockedStatic<BukkitAdapter> bukkitAdapterMock = mockStatic(BukkitAdapter.class)) {
				paperLibMock.when(PaperLib::getMinecraftVersion).thenReturn(20);
				final com.sk89q.worldedit.world.World worldEditWorld = mock(com.sk89q.worldedit.world.World.class);
				bukkitAdapterMock.when(() -> BukkitAdapter.adapt(world)).thenReturn(worldEditWorld);
				final WorldEditPlugin worldEditPlugin = mock(WorldEditPlugin.class);
				when(plugin.getWorldEdit()).thenReturn(worldEditPlugin);
				when(worldEditPlugin.getSession(player)).thenReturn(localSession);
				when(player.getWorld()).thenReturn(world);
				when(localSession.getSelection(worldEditWorld)).thenThrow(new IncompleteRegionException());

				// when
				final String result = command.create("test", 5);

				// then
				verify(plugin).getGames();
				assertThat(result).isEqualTo("&4Error: &cRegion is incomplete");
			}
		}
	}

	@Test
	void testCreateRegionIsNull() throws IncompleteRegionException {
		try (final MockedStatic<Bukkit> bukkitMock = mockStatic(Bukkit.class)) {
			// given
			final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
			bukkitMock.when(Bukkit::getVersion).thenReturn("(MC: 1.19.4)");
			try (final MockedStatic<PaperLib> paperLibMock = mockStatic(PaperLib.class);
				 final MockedStatic<BukkitAdapter> bukkitAdapterMock = mockStatic(BukkitAdapter.class)) {
				paperLibMock.when(PaperLib::getMinecraftVersion).thenReturn(20);
				final com.sk89q.worldedit.world.World worldEditWorld = mock(com.sk89q.worldedit.world.World.class);
				bukkitAdapterMock.when(() -> BukkitAdapter.adapt(world)).thenReturn(worldEditWorld);
				final WorldEditPlugin worldEditPlugin = mock(WorldEditPlugin.class);
				when(plugin.getWorldEdit()).thenReturn(worldEditPlugin);
				when(worldEditPlugin.getSession(player)).thenReturn(localSession);
				when(player.getWorld()).thenReturn(world);
				when(localSession.getSelection(worldEditWorld)).thenReturn(null);

				// when
				final String result = command.create("test", 5);

				// then
				verify(plugin).getGames();
				assertThat(result).isEqualTo("&4Error: &cRegion doesn't exist");
			}
		}
	}

	@Test
	void testCreateCollidingGame() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		when(plugin.getGames()).thenReturn(Collections.singletonList(new VillagerDefenseGame(
				new VillagerDefenseCuboidRegion(BlockVector3.ZERO, BlockVector3.ZERO), "game1", 15)));

		// when
		final String result = command.create("game1", 5);

		// then
		verify(plugin).getGames();
		assertThat(result).isEqualTo("&4Error: &cGame of that name already exists");
	}

	@Test
	void testAddMobSuccessfully() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(BlockVector3.at(3, 65, 5),
				BlockVector3.at(10, 70, 10));
		final VillagerDefenseGame villagerDefenseGame = new VillagerDefenseGame(cuboidRegion, "test", 10);
		final Location location = new Location(world, 4, 67, 7);
		when(player.getLocation()).thenReturn(location);

		// when
		final String result = command.add(villagerDefenseGame, EntityType.CREEPER, "testCreeper");

		// then
		assertThat(villagerDefenseGame.getMobSpawnPoints())
				.containsExactly(new MobSpawnLocation(EntityType.CREEPER, location, "testCreeper"));
		assertThat(result).isEqualTo("&aSuccessfully added mob to the game!");
	}

	@Test
	void testAddCollidingMob() {
		// given
		final VillagerDefenseCommand command = new VillagerDefenseCommand(player, plugin);
		final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(BlockVector3.at(3, 65, 5),
				BlockVector3.at(10, 70, 10));
		final VillagerDefenseGame villagerDefenseGame = new VillagerDefenseGame(cuboidRegion, "test", 10);
		villagerDefenseGame.getMobSpawnPoints().add(new MobSpawnLocation(EntityType.CREEPER,
				new Location(world, 0, 0, 0), "testCreeper"));

		// when
		final String result = command.add(villagerDefenseGame, EntityType.CREEPER, "testCreeper");

		// then
		assertThat(result).isEqualTo("&4Error: &cMob of that name already exists");
	}
}
