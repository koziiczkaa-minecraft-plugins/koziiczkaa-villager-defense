package com.alicjakozik.spigot.koziiczkaavillagerdefense;

import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.MobSpawnLocation;
import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.VillagerDefenseCuboidRegion;
import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.VillagerDefenseGame;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.api.annotation.Argument;
import eu.andret.arguments.api.annotation.BaseCommand;
import eu.andret.arguments.api.annotation.TypeFallback;
import eu.andret.arguments.api.entity.ExecutorType;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.metadata.FixedMetadataValue;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.Generated;
import java.util.List;

@BaseCommand("villagerdefense")
public class VillagerDefenseCommand extends AnnotatedCommandExecutor<VillagerDefensePlugin> {
	private static final String NAME = "${NAME}";

	public VillagerDefenseCommand(final CommandSender sender, final VillagerDefensePlugin plugin) {
		super(sender, plugin);
	}

	@NotNull
	@TypeFallback(VillagerDefenseGame.class)
	public String villagerDefenseGameFallback(@NotNull final String name) {
		return "&4Error: &cCouldn't find game called &6${NAME}".replace(NAME, name);
	}

	@NotNull
	@Argument(permission = "vd.region", executorType = ExecutorType.PLAYER)
	public String create(@NotNull final String gameName, final int maxPlayers) {
		final List<VillagerDefenseGame> games = plugin.getGames();
		final boolean gameExists = games.stream().map(VillagerDefenseGame::getName).anyMatch(name -> name.equals(gameName));
		if (gameExists) {
			return "&4Error: &cGame of that name already exists";
		}
		final Player player = (Player) sender;
		final LocalSession session = plugin.getWorldEdit().getSession(player);
		try {
			final com.sk89q.worldedit.world.World world = BukkitAdapter.adapt(player.getWorld());
			final Region region = session.getSelection(world);
			if (region == null) {
				return "&4Error: &cRegion doesn't exist";
			}
			final VillagerDefenseCuboidRegion cuboidRegion = new VillagerDefenseCuboidRegion(world,
					region.getMaximumPoint(), region.getMinimumPoint());
			games.add(new VillagerDefenseGame(cuboidRegion, gameName, maxPlayers));
			return "&aSuccessfully created region!";
		} catch (final IncompleteRegionException e) {
			return "&4Error: &cRegion is incomplete";
		}
	}

	@NotNull
	@Argument(permission = "vd.villager", executorType = ExecutorType.PLAYER)
	public String villager(@NotNull final VillagerDefenseGame game) {
		final Player player = (Player) sender;
		final Location location = player.getLocation();
		final World world = location.getWorld();
		if (world == null) {
			return "&4Error: &cWorld doesn't exist";
		}
		final BlockVector3 position = BlockVector3.at(location.getX(), location.getY(), location.getZ());
		final boolean isPlayerInRegion = game.getCuboidRegion().contains(position);
		if (!isPlayerInRegion) {
			return "&4Error: &cYou have to be in the game's region to perform this command!";
		}
		if (game.getVillager() != null) {
			game.getVillager().remove();
		}
		final Villager villager = world.spawn(location, Villager.class);
		villager.setMetadata("villagerDefenseGame", new FixedMetadataValue(plugin, game.getName()));
		villager.setAI(false);
		game.setVillager(villager);
		return "&aSuccessfully added villager to the game!";
	}

	@Argument(permission = "vd.add", executorType = ExecutorType.PLAYER)
	public String add(@NotNull final VillagerDefenseGame game, @NotNull final EntityType entity,
					  @NotNull final String mobName) {
		final List<MobSpawnLocation> mobSpawnPoints = game.getMobSpawnPoints();
		final boolean mobExists = mobSpawnPoints.stream().map(MobSpawnLocation::name).anyMatch(s -> s.equals(mobName));
		if (mobExists) {
			return "&4Error: &cMob of that name already exists";
		}
		final Player player = (Player) sender;
		mobSpawnPoints.add(new MobSpawnLocation(entity, player.getLocation(), mobName));
		return "&aSuccessfully added mob to the game!";
	}

	@Generated("Test command")
	@Argument(executorType = ExecutorType.PLAYER)
	public String spawnDebug(@NotNull final VillagerDefenseGame game) {
		game.getMobSpawnPoints().forEach(mobSpawnLocation -> {
			final Location location = mobSpawnLocation.location();
			final World world = location.getWorld();
			if (world == null) {
				return;
			}
			final Class<? extends Entity> entityClass = mobSpawnLocation.entityType().getEntityClass();
			if (entityClass == null) {
				return;
			}
			final LivingEntity entity = (LivingEntity) world.spawn(location, entityClass);
			entity.setAI(false);
		});
		return "&aGame started";
	}
}
