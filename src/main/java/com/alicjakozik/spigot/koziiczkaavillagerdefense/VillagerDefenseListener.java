package com.alicjakozik.spigot.koziiczkaavillagerdefense;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.metadata.MetadataValue;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VillagerDefenseListener implements Listener {
	@EventHandler
	public void onEntityDamage(@NotNull final EntityDamageByEntityEvent event) {
		final List<MetadataValue> villagerMetadata = event.getEntity().getMetadata("villagerDefenseGame");
		if (!villagerMetadata.isEmpty()) {
			event.setCancelled(true);
		}
	}
}
