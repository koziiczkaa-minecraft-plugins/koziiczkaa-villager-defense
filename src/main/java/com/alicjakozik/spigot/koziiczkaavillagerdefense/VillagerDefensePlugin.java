package com.alicjakozik.spigot.koziiczkaavillagerdefense;

import com.alicjakozik.spigot.koziiczkaavillagerdefense.entity.VillagerDefenseGame;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import eu.andret.arguments.api.entity.FallbackConstants;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class VillagerDefensePlugin extends JavaPlugin {
	private final List<VillagerDefenseGame> games = new ArrayList<>();

	@Override
	public void onEnable() {
		setupCommand();
		getServer().getPluginManager().registerEvents(new VillagerDefenseListener(), this);
	}

	public void setupCommand() {
		final AnnotatedCommand<VillagerDefensePlugin> command =
				CommandManager.registerCommand(VillagerDefenseCommand.class, this);
		command.setOnUnknownSubCommandExecutionListener(commandSender ->
				commandSender.sendMessage("&4Error: &cNo such command"));
		command.addTypeMapper(VillagerDefenseGame.class, this::findGame, FallbackConstants.ON_NULL);
		command.addTypeMapper(EntityType.class, VillagerDefensePlugin::findEntity, FallbackConstants.ON_NULL);
		command.addTypeCompleter(VillagerDefenseGame.class, this::getGameNames);
		command.addTypeCompleter(EntityType.class, VillagerDefensePlugin::getEntityNames);
		command.getOptions().setAutoTranslateColors(true);
	}

	@NotNull
	public WorldEditPlugin getWorldEdit() {
		return getPlugin(WorldEditPlugin.class);
	}

	@NotNull
	public List<VillagerDefenseGame> getGames() {
		return games;
	}

	@Nullable
	private VillagerDefenseGame findGame(@Nullable final String name) {
		return games.stream()
				.filter(villagerDefenseGame -> villagerDefenseGame.getName().equals(name))
				.findAny()
				.orElse(null);
	}

	@Nullable
	private static EntityType findEntity(@Nullable final String entity) {
		return Arrays.stream(EntityType.values())
				.filter(EntityType::isAlive)
				.filter(entityType -> entityType.name().equals(entity))
				.findAny()
				.orElse(null);
	}

	@NotNull
	private Collection<String> getGameNames(@NotNull final CommandSender commandSender,
											@NotNull final Collection<String> strings) {
		return games.stream()
				.map(VillagerDefenseGame::getName)
				.filter(name -> name.startsWith(new ArrayList<>(strings).get(strings.size() - 1)))
				.toList();
	}

	@NotNull
	private static Collection<String> getEntityNames(@NotNull final CommandSender commandSender,
													 @NotNull final Collection<String> strings) {
		return Arrays.stream(EntityType.values())
				.filter(EntityType::isAlive)
				.map(Enum::name)
				.filter(name -> name.startsWith(new ArrayList<>(strings).get(strings.size() - 1)))
				.toList();
	}
}
