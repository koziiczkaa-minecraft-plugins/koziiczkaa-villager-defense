package com.alicjakozik.spigot.koziiczkaavillagerdefense.entity;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.jetbrains.annotations.NotNull;

public record MobSpawnLocation(@NotNull EntityType entityType, @NotNull Location location, String name) {
}
