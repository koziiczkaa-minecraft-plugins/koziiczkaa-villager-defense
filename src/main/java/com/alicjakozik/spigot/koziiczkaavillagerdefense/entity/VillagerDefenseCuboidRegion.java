package com.alicjakozik.spigot.koziiczkaavillagerdefense.entity;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.world.World;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class VillagerDefenseCuboidRegion extends CuboidRegion {
	public VillagerDefenseCuboidRegion(@NotNull final BlockVector3 pos1, @NotNull final BlockVector3 pos2) {
		this(null, pos1, pos2);
	}

	public VillagerDefenseCuboidRegion(@Nullable final World world, @NotNull final BlockVector3 pos1,
									   @NotNull final BlockVector3 pos2) {
		super(world, pos1, pos2);
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof final VillagerDefenseCuboidRegion that)) {
			return false;
		}
		return Objects.equals(getPos1(), that.getPos1())
				&& Objects.equals(getPos2(), that.getPos2());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getPos1(), getPos2());
	}
}
