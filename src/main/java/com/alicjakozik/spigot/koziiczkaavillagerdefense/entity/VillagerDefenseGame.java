package com.alicjakozik.spigot.koziiczkaavillagerdefense.entity;

import com.sk89q.worldedit.regions.CuboidRegion;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VillagerDefenseGame {
	private final VillagerDefenseCuboidRegion cuboidRegion;
	private final String name;
	private final int maxPlayers;
	private final List<Player> players = new ArrayList<>();
	private int wave = 0;
	private Villager villager;
	private final List<MobSpawnLocation> mobSpawnPoints = new ArrayList<>();

	public VillagerDefenseGame(@NotNull final VillagerDefenseCuboidRegion cuboidRegion, @NotNull final String name, final int maxPlayers) {
		this.cuboidRegion = cuboidRegion;
		this.name = name;
		this.maxPlayers = maxPlayers;
	}

	@NotNull
	public CuboidRegion getCuboidRegion() {
		return cuboidRegion;
	}

	@NotNull
	public String getName() {
		return name;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public int getWave() {
		return wave;
	}

	@Nullable
	public Villager getVillager() {
		return villager;
	}

	@NotNull
	public List<MobSpawnLocation> getMobSpawnPoints() {
		return mobSpawnPoints;
	}

	public void setWave(final int wave) {
		this.wave = wave;
	}

	public void setVillager(@Nullable final Villager villager) {
		this.villager = villager;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final VillagerDefenseGame that)) {
			return false;
		}
		return maxPlayers == that.maxPlayers
				&& wave == that.wave
				&& Objects.equals(cuboidRegion, that.cuboidRegion)
				&& Objects.equals(name, that.name)
				&& Objects.equals(players, that.players);
	}

	@Override
	public int hashCode() {
		return Objects.hash(cuboidRegion, name, maxPlayers, players, wave);
	}

	@Override
	public String toString() {
		return "VillagerDefenseGame{" +
				"cuboidRegion=" + cuboidRegion +
				", name='" + name + '\'' +
				", maxPlayers=" + maxPlayers +
				", players=" + players +
				", wave=" + wave +
				'}';
	}
}
